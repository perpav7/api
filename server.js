//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');

var path = require('path');

var urlMLab = "https://api.mlab.com/api/1/databases/bdbanca3mb64186/collections/";
var apiKey = "?apiKey=RxAWh38_IC24E684Oy033oPGQqUDo6ft";
var reqMLab;

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res){
  //res.send('Hola mundo node');
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/login',function(req, res){
  var url = urlMLab + "clientes" + apiKey;
  reqMLab = requestjson.createClient(url);
  reqMLab.get('', req.body, function(err, resM, body){
    var isOk = false;
    var j;
    var datosLogin = {'nombreCliente': '', 'numCliente': ''};
    if(err){
      console.log(err);
    }else{
      for(i=0; i<body.length; i++){
        if(req.body.username === body[i].usuario && req.body.password === body[i].contrasena){
          isOk = true;
          j=i;
        }
      }
      if(isOk){
        datosLogin.nombreCliente = body[j].nombre;
        datosLogin.numCliente = body[j].idcliente;
        res.send(datosLogin);
      }else{
        res.send(isOk);
      }
    }
  });
});

app.post('/posicionglobal',function(req, res){
  var urlcontent = "&q={\"cliente\":\""+req.body.numCliente+"\"}";
  var url = urlMLab + "cuentas" + apiKey + urlcontent;
  reqMLab = requestjson.createClient(url);
  reqMLab.get('', function(err, resM, body){
      if(err){
        console.log(err);
      }else{
        res.send(body);
      }
    });
});

app.post('/movimientos',function(req, res){
  var urlcontent = "&q={\"cliente\":\""+req.body.cliente+"\",\"numCuenta\":\""+req.body.cuenta+"\"}";
  var url = urlMLab + "movimientos" + apiKey + urlcontent;
  reqMLab = requestjson.createClient(url);
  reqMLab.get('', function(err, resM, body){
      if(err){
        console.log(err);
      }else{
        res.send(body);
      }
    });
});

app.post('/altamovimientos',function(req, res){
  var reqTransf = req.body;
  var urlcontent = "&q={\"cliente\":\""+req.body.cliente+"\",\"numCuenta\":\""+req.body.numCuenta+"\"}";
  var ulrConsCta = urlMLab + "cuentas" + apiKey + urlcontent;
  var urlImp = urlMLab + "cuentas" + apiKey;
  var url = urlMLab + "movimientos" + apiKey;
  var importe = parseFloat(req.body.cargo);
  reqMLab = requestjson.createClient(url);
  reqMLab.post('', reqTransf, function(err, resM, body){
      if(err){
        console.log(err);
      }else{
        reqMLab = requestjson.createClient(ulrConsCta);
        reqMLab.get('', req.body, function(err, resM, body){
            if(err){
              console.log(err);
            }else{
              var saldoDisponible = parseFloat(body[0].saldoDisponible);
              saldoDisponible -= importe;
              var parAltaMov = {'cliente': body[0].cliente,'tipoCuenta': body[0].tipoCuenta,'numCuenta': body[0].numCuenta,'aliasCuenta': body[0].aliasCuenta,'moneda': body[0].moneda,'saldoDisponible': saldoDisponible};
              reqMLab = requestjson.createClient(urlImp);
              reqMLab.put('', parAltaMov, function(err, resM, body){
                  if(err){
                    console.log(err);
                  }else{
                    res.send(body);
                  }
                });
            }
          });
      }
    });
});

app.post('/registrar',function(req, res){
  console.log(req.body.usuario);
  var urlcontent = "&q={\"usuario\":\""+req.body.usuario+"\"}";
  var url = urlMLab + "clientes" + apiKey + urlcontent;
  reqMLab = requestjson.createClient(url);
  reqMLab.get('', function(err, resM, body){
      if(err){
        console.log(err);
      }else{
        if(body.length == 0){
          var reqReg = requestjson.createClient(urlMLab + "clientes" + apiKey);
          var newIdCliente = "A" + Math.floor(Math.random() * 10000000 + 1).toString();
          var newbodyReg = {'nombre': req.body.nombre,'apellido': req.body.apellido,'idcliente': newIdCliente,'usuario': req.body.usuario,'contrasena': req.body.contrasena};
          reqReg.post('', newbodyReg, function(err, resM, body){
              if(err){
                console.log(err);
              }else{
                res.send(body);
              }
            });
        }else{
          res.send(false);
        }
      }
    });
});
